/// <reference path="../typings/tsd.d.ts" />
var cheerio = require("cheerio");
var fs = require("fs");
var moment = require("moment");
function formatName(name) {
    return name.trim()
        .replace(/ ап$/, " преф")
        .replace(/ п$/, " преф")
        .replace(/-п$/, " преф")
        .replace(/-ап$/, " преф")
        .replace(/ \(?ОАО\)?/, "")
        .replace(/\s+/, "")
        .toLowerCase();
}
function readTickers() {
    var rows = fs.readFileSync("./data/sec-codes.csv", "utf-8").split("\n");
    return rows.splice(1).map(function (row) {
        var cols = row.split(",");
        return [cols[0].trim(), formatName(cols[1].trim()), formatName(cols[2].trim())];
    });
}
function findTickerCodeByName(name) {
    var row = tickers.filter(function (f) { return f[1].indexOf(name) != -1 || f[2].indexOf(name) != -1; })[0];
    return row ? row[0] : null;
}
var tickers = readTickers();
function getOrder(name) {
    return name == "Покупать" ? "buy" : "sell";
}
function parse(html) {
    var $ = cheerio.load(html);
    var items = $(".views-table.cols-6 tr")
        .map(function () { return $(this).children("td").text(); });
    return items.get()
        .filter(function (f) { return f; })
        .map(function (m) { return m.split('\n').map(function (x) { return x.trim(); }); })
        .map(function (m) {
        var name = m[2].trim().toLowerCase();
        var filterName = name.replace(/\s+/, "");
        return {
            securityCode: findTickerCodeByName(filterName),
            securityName: name,
            date: moment.utc(m[1], "DD.MM.YYYY - HH:mm"),
            order: getOrder(m[3]),
            force: parseInt(m[4]),
            stop: parseFloat(m[5])
        };
    });
}
exports.parse = parse;
;
