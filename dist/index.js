/// <reference path="../typings/tsd.d.ts" />
var rabbit = require("da-rabbitmq-rx");
var logs = require("da-logs");
var handler = require("./handler");
var trAnt = require("tr-ant-utils");
var getEnvVar = trAnt.getEnvVar;
var RABBIT_URI = getEnvVar("RABBIT_URI");
var RABBIT_QUEUE_CMDS = getEnvVar("RABBIT_QUEUE_COMMANDS");
var RABBIT_QUEUE_NOTIFS = getEnvVar("RABBIT_QUEUE_NOTIFS");
var RABBIT_QUEUE_QUOTES = getEnvVar("RABBIT_QUEUE_QUOTES");
var LOG_LOGGLY_KEY = getEnvVar("LOG_LOGGLY_KEY");
var LOG_LOGGLY_SUBDOMAIN = getEnvVar("LOG_LOGGLY_SUBDOMAIN");
var LOG_MONGO_URI = getEnvVar("LOG_MONGO_URI");
var LOG_MONGO_COLLECTION = getEnvVar("LOG_MONGO_COLLECTION");
var SILVER_SURFER_NFR_PARSE_PAGE_URL = getEnvVar("SILVER_SURFER_NFR_PARSE_PAGE_URL");
var SILVER_SURFER_NFR_PARSE_INTERVAL = parseInt(getEnvVar("SILVER_SURFER_NFR_PARSE_INTERVAL"));
var NFR_SILVER_SURFER_START_DUE_TIME = parseInt(getEnvVar("NFR_SILVER_SURFER_START_DUE_TIME"));
var logger = new logs.LoggerCompose({ pack: require("../package.json"), tags: [] }, {
    loggly: { token: LOG_LOGGLY_KEY, subdomain: LOG_LOGGLY_SUBDOMAIN },
    mongo: { connection: LOG_MONGO_URI, collection: LOG_MONGO_COLLECTION },
    console: true
});
var pubOpts = { uri: RABBIT_URI, socketType: rabbit.SocketType.PUB, queue: RABBIT_QUEUE_NOTIFS };
var pub = new rabbit.RabbitPub(pubOpts);
pub.connect();
pub.connectStream.subscribe(function () {
    logger.write({ resource: "rabbit", oper: "connected", status: "success", opts: pubOpts });
}, function (err) {
    logger.write({ resource: "rabbit", status: "error", err: err, opts: pubOpts });
    process.exit(1);
});
var handlerOpts = {
    pageUrl: SILVER_SURFER_NFR_PARSE_PAGE_URL,
    parseInterval: SILVER_SURFER_NFR_PARSE_INTERVAL,
    startDueTime: NFR_SILVER_SURFER_START_DUE_TIME,
    logger: logger,
    pub: pub
};
handler.handle(handlerOpts);
