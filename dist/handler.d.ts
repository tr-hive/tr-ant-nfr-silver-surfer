/// <reference path="../typings/tsd.d.ts" />
import logs = require("da-logs");
import rabbit = require("da-rabbitmq-rx");
export interface IHandlerOpts {
    logger: logs.ILogger;
    pub: rabbit.RabbitPub;
    pageUrl: string;
    parseInterval: number;
    startDueTime: number;
    newDate?(): Date;
    newUUID?(): string;
}
export declare function handle(opts: IHandlerOpts): any;
