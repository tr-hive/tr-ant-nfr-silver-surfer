/// <reference path="../typings/tsd.d.ts" />
export interface ISilverSurferRow {
    securityCode?: string;
    securityName: string;
    date: moment.Moment;
    order: string;
    force?: number;
    stop?: number;
}
export declare function parse(html: string): ISilverSurferRow[];
