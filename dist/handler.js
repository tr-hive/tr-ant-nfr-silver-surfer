/// <reference path="../typings/tsd.d.ts" />
var Rx = require("rx");
var uuid = require("node-uuid");
var trAnt = require("tr-ant-utils");
var request = require("request");
var moment = require("moment");
var parser = require("./parser");
var PKG_NAME = require("../package.json").name;
var newDate = function () { return new Date(); };
var newUUID = function () { return uuid.v4(); };
function mapRow(ssRow, date) {
    return {
        key: ssRow.date + "_" + ssRow.securityCode + "_" + PKG_NAME,
        issuer: PKG_NAME,
        date: newDate().toISOString(),
        type: "INotifSilverSurferData",
        data: {
            ticket: ssRow.securityCode,
            oper: ssRow.order,
            stop: ssRow.stop,
            force: ssRow.force
        }
    };
}
function createParseStream(pageUrl, logger) {
    return Rx.Observable.fromNodeCallback(request)(pageUrl)
        .do(function (val) { return logger.write({ oper: "handle", status: "start" }); })
        .map(function (val) { return parser.parse(val[1]); })
        .selectMany(function (val) { return Rx.Observable.fromArray(val.reverse()); })
        .map(function (val) { return trAnt.tulpe(val, mapRow(val, newDate())); });
}
function handle(opts) {
    if (opts.newDate)
        newDate = opts.newDate;
    if (opts.newUUID)
        newUUID = opts.newUUID;
    var latestDate = moment.utc().startOf("d");
    var intervalStream = Rx.Observable.timer(opts.startDueTime, opts.parseInterval)
        .selectMany(function (_) { return createParseStream(opts.pageUrl, opts.logger); });
    intervalStream.subscribe(function (val) {
        if (latestDate.isBefore(val.val1.date)) {
            latestDate = val.val1.date;
            val.val1.date = val.val1.date.toISOString();
            opts.logger.write({ oper: "handle", status: "success", notif: val.val2, row: val.val1 });
            opts.pub.write(val.val2);
        }
    });
    return intervalStream;
}
exports.handle = handle;
