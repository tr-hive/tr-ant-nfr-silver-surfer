/// <reference path="../typings/tsd.d.ts" />
var cheerio = require("cheerio");
var fs = require("fs");
var moment = require("moment");
import trAnt = require("tr-ant-utils");

export interface ISilverSurferRow {
    securityCode? : string
    securityName: string
    date : moment.Moment
    order: string //buy | sell
    force? : number
    stop? : number  
}

function formatName(name: string) : string {
  return name.trim()
  .replace(/ ап$/, " преф")
  .replace(/ п$/, " преф")
  .replace(/-п$/, " преф")
  .replace(/-ап$/, " преф")
  .replace(/ \(?ОАО\)?/, "")
  .replace(/\s+/, "")
  .toLowerCase();
}

//row rows with 3 columns: code, short name, full name
function readTickers() : string[][] {
  var rows = fs.readFileSync("./data/sec-codes.csv", "utf-8").split("\n");
  return rows.splice(1).map((row) => {
    var cols = row.split(",");
    return [cols[0].trim(), formatName(cols[1].trim()), formatName(cols[2].trim())];
  });         
}  

function findTickerCodeByName(name: string) : string {
   
  var row = tickers.filter((f) => f[1].indexOf(name) != -1 || f[2].indexOf(name) != -1)[0];
  
  return row ? row[0] : null; 
}

var tickers = readTickers();

function getOrder(name) {
  return name == "Покупать" ? "buy" : "sell";
}

export function parse(html: string) : ISilverSurferRow[]  {
  
  var $ = cheerio.load(html);
  
  var items = $(".views-table.cols-6 tr")
    .map(function () { return $(this).children("td").text(); });
                  
  return items.get()
  .filter((f) => f)
  .map((m) => m.split('\n').map((x) => x.trim()))
  .map((m) => { 
    var name = m[2].trim().toLowerCase();
    var filterName = name.replace(/\s+/, "");
    return {
      securityCode : findTickerCodeByName(filterName),
      securityName: name,
      date : moment.utc(m[1], "DD.MM.YYYY - HH:mm"),
      order: getOrder(m[3]),
      force : parseInt(m[4]),
      stop : parseFloat(m[5])
    }}
  )
  
};