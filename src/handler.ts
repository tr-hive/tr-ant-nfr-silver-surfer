/// <reference path="../typings/tsd.d.ts" />
import Rx = require("rx");
import logs = require("da-logs");
import rabbit = require("da-rabbitmq-rx");
var uuid = require("node-uuid");
import trAnt =require("tr-ant-utils");
import request = require("request");
import moment = require("moment");
import parser = require("./parser");

const PKG_NAME = require("../package.json").name;

var newDate = () => new Date();  
var newUUID = () => uuid.v4();

function mapRow(ssRow: parser.ISilverSurferRow, date: Date) : trAnt.INotif<trAnt.INotifSilverSurferData> {
      return {
            key: ssRow.date  + "_" + ssRow.securityCode + "_"  + PKG_NAME,
            issuer: PKG_NAME,
            date: newDate().toISOString(),
            type: "INotifSilverSurferData",
            data : {
                  ticket: ssRow.securityCode,
                  oper: ssRow.order,
                  stop: ssRow.stop,
                  force: ssRow.force              
            }                     
      }      
} 

export interface IHandlerOpts {
	logger: logs.ILogger 	
	pub: rabbit.RabbitPub  
      pageUrl: string
      parseInterval: number
      startDueTime: number
	newDate?() : Date
	newUUID?() : string  
}

function createParseStream(pageUrl: string, logger: logs.ILogger) {
       return Rx.Observable.fromNodeCallback(request)(pageUrl)
            .do(val => logger.write({oper: "handle", status: "start"}))
            .map(val => parser.parse(val[1]))
            .selectMany(val => Rx.Observable.fromArray(val.reverse()))
            .map(val => trAnt.tulpe(val, mapRow(val, newDate())));
}

export function handle(opts : IHandlerOpts) : any {
	if (opts.newDate)
		newDate = opts.newDate;
	if (opts.newUUID)		
		newUUID = opts.newUUID;
            
      var latestDate = moment.utc().startOf("d");          
            		
      var intervalStream = Rx.Observable.timer(opts.startDueTime, opts.parseInterval)
      .selectMany(_ => createParseStream(opts.pageUrl, opts.logger));
                                               
      intervalStream.subscribe(val => {                    
          if (latestDate.isBefore(val.val1.date)) {
            latestDate = val.val1.date;
            (<any>val.val1).date = val.val1.date.toISOString(); 
            opts.logger.write({oper: "handle", status: "success", notif : val.val2, row : val.val1}); 
            opts.pub.write(val.val2);            
          }          
      });                                    
      
      return intervalStream;           					
}


