 /// <reference path="../typings/tsd.d.ts" />
import rabbit = require("da-rabbitmq-rx");
import logs = require("da-logs");
import handler = require("./handler");
import trAnt = require("tr-ant-utils");
 
import getEnvVar = trAnt.getEnvVar;

const RABBIT_URI = getEnvVar("RABBIT_URI");
const RABBIT_QUEUE_CMDS = getEnvVar("RABBIT_QUEUE_COMMANDS");
const RABBIT_QUEUE_NOTIFS = getEnvVar("RABBIT_QUEUE_NOTIFS");
const RABBIT_QUEUE_QUOTES = getEnvVar("RABBIT_QUEUE_QUOTES");
const LOG_LOGGLY_KEY = getEnvVar("LOG_LOGGLY_KEY");
const LOG_LOGGLY_SUBDOMAIN = getEnvVar("LOG_LOGGLY_SUBDOMAIN");
const LOG_MONGO_URI = getEnvVar("LOG_MONGO_URI");
const LOG_MONGO_COLLECTION = getEnvVar("LOG_MONGO_COLLECTION");
const SILVER_SURFER_NFR_PARSE_PAGE_URL=getEnvVar("SILVER_SURFER_NFR_PARSE_PAGE_URL");
const SILVER_SURFER_NFR_PARSE_INTERVAL=parseInt(getEnvVar("SILVER_SURFER_NFR_PARSE_INTERVAL"));
const NFR_SILVER_SURFER_START_DUE_TIME=parseInt(getEnvVar("NFR_SILVER_SURFER_START_DUE_TIME"));

var logger = new logs.LoggerCompose({pack : <any>require("../package.json"), tags : []},  {
    loggly: {token: LOG_LOGGLY_KEY, subdomain: LOG_LOGGLY_SUBDOMAIN},
    mongo: {connection: LOG_MONGO_URI, collection: LOG_MONGO_COLLECTION},
    console: true
 });

var pubOpts = {uri: RABBIT_URI, socketType: rabbit.SocketType.PUB, queue: RABBIT_QUEUE_NOTIFS};
var pub = new rabbit.RabbitPub(pubOpts); 
pub.connect();
pub.connectStream.subscribe(() => {
 logger.write({resource: "rabbit", oper: "connected", status : "success", opts: pubOpts});
} 
, (err) => {
	logger.write({resource: "rabbit", status : "error", err: err, opts: pubOpts});
	process.exit(1);
});

var handlerOpts : handler.IHandlerOpts = {
	pageUrl: SILVER_SURFER_NFR_PARSE_PAGE_URL,
	parseInterval:SILVER_SURFER_NFR_PARSE_INTERVAL,	
	startDueTime: NFR_SILVER_SURFER_START_DUE_TIME,
	logger: logger, 	
	pub: pub	
};

handler.handle(handlerOpts);